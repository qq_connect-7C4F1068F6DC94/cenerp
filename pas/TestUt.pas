unit TestUt;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,deviceTool,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.ScrollBox,
  {$IFDEF ANDROID}
     Androidapi.Helpers,
      Androidapi.JNI.GraphicsContentViewText,
 {$ENDIF}
 FMX.Memo, FMX.Colors;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    ColorBox1: TColorBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
begin
   deviceTool.getScreen(true);
   Memo1.Lines.Clear;
   Memo1.Lines.Add(deviceTool.screenwidth.ToString);
   Memo1.Lines.Add(deviceTool.screenHeight.ToString);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  {$IFDEF ANDROID}
  //TAndroidHelper.Activity.getWindow.setStatusBarColor(TJColor.JavaClass.RED);
  TAndroidHelper.Activity.getWindow.setStatusBarColor(ColorBox1.Color);
  {$ENDIF}
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
//  {$IFDEF ANDROID}
//    TAndroidHelper.Activity.getWindow.setStatusBarColor(TJColor.JavaClass.YELLOW);
//  {$ENDIF}

  {$IFDEF ANDROID}
  TAndroidHelper.Activity.getWindow.setStatusBarColor(TJColor.JavaClass.RED);
  {$ENDIF}
end;

end.
