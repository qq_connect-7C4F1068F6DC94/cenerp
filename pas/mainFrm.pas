unit mainFrm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.Objects,FMX.PlatForm, FMX.Layouts,startFrm,
  System.ImageList, FMX.ImgList,mainFrame,myFrame,orderFrame,countUt,deviceTool;

type
  TmainForm = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Button2: TButton;
    BottomLayout: TGridLayout;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    i1: TImage;
    i2: TImage;
    t2: TText;
    i3: TImage;
    t3: TText;
    t4: TText;
    i4: TImage;
    ImageList1: TImageList;
    ImageList2: TImageList;
    mainLayout: TVertScrollBox;
    t1: TText;
    Timer1: TTimer;

    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure i4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
     timecount: Integer;
     itemw: Integer;
     procedure frameShow(iframe: Byte);
     procedure startFormShow;
     function HandleAppEvent(AAppEvent: TApplicationEvent;
      AContext: TObject): Boolean;
  public
    { Public declarations }
  end;

var
  mainForm: TmainForm;
  mFrame: TmFrame;
  mysFrame: TmysFrame;
  countFrm: TcountFrm;
implementation
 uses {$IFDEF ANDROID}
     Androidapi.Helpers,
      Androidapi.JNI.GraphicsContentViewText,
 {$ENDIF}
 loginFrm;
{$R *.fmx}

procedure TmainForm.Button1Click(Sender: TObject);
var loginForm : TloginForm;
begin
   //
   loginForm := TloginForm.Create(nil);
   loginForm.ShowModal;
   loginForm.ShowModal(
     procedure(ModalResult : TModalResult)
                     begin
                          if ModalResult = mrOK then
                          begin
                            Self.Edit1.Text := 'mrOk';
                          end;
                          // ShowMessage('mrOK');
                          if ModalResult = mrClose then
                          begin
                            Self.Edit1.Text := 'mrClose';
                          end;
                           if ModalResult = mrNone then
                          begin
                            Self.Edit1.Text := 'mrNone';
                          end;
//                          else
//                          begin
//                             Self.Edit1.Text := ModalResult;
//                          end;
                           // ShowMessage('mrClose');
                     end);
   FreeAndNil(loginForm);
end;

procedure TmainForm.Button2Click(Sender: TObject);
var
  SvcEvents: IFMXApplicationEventService;
begin
   //
    if TPlatformServices.Current.SupportsPlatformService
    (IFMXApplicationEventService, IInterface(SvcEvents))
  then
    SvcEvents.SetApplicationEventHandler(HandleAppEvent);
end;

procedure TmainForm.FormCreate(Sender: TObject);
var
  SvcEvents: IFMXApplicationEventService;

begin
     {$IFDEF Android}
//     timecount := 0;
//     Timer1.Enabled := True;
//    if TPlatformServices.Current.SupportsPlatformService
//    (IFMXApplicationEventService, IInterface(SvcEvents))
//    then
//    SvcEvents.SetApplicationEventHandler(HandleAppEvent);
     if getScreen(False) then
     begin
       mainForm.Width := Trunc(screenWidth);
       itemw := mainForm.Width div 4;
      //itemw := 392
       BottomLayout.ItemWidth := itemw;
     end;

    {$ENDIF}

   {$IFDEF ANDROID}
  //TAndroidHelper.Activity.getWindow.setStatusBarColor(TJColor.JavaClass.YELLOW);
   setBarColor(False);
  {$ENDIF}
end;

procedure TmainForm.FormShow(Sender: TObject);
begin
   //
      //  setBarColor(False);
   i4Click(i1);
end;

procedure TmainForm.frameShow(iframe: Byte);
begin
  //
  case iframe of
    0: begin
            if countFrm <> nil then
            countFrm.Parent := nil;
          if mysFrame <> nil then
             mysFrame.Parent := nil;
          if mFrame = nil then
             mFrame :=  TmFrame.Create(nil);
          mFrame.Parent := mainLayout;
          mFrame.Width := mainLayout.Width;
          mFrame.Height := mainLayout.Height;
          {$IFDEF Android}
          mFrame.GridLayout1.ItemWidth := itemw;
          mFrame.GridLayout2.ItemWidth := itemw;
          mFrame.Rectangle2.Width := mFrame.Width;
          mFrame.Rectangle4.Width := mFrame.Width;
          {$ENDIF}
       end;
    1: begin

       end;
    2: begin
         if countFrm = nil then
            countFrm := TcountFrm.Create(mainLayout);
         if mFrame <> nil then
            mFrame.Parent := nil;
         if mysFrame <> nil then
            mysFrame.Parent := nil;

         countFrm.Parent := mainLayout;
         countFrm.Width := mainLayout.Width;
         countFrm.Height := mainLayout.Height;
       end;
    3: begin
           if countFrm <> nil then
            countFrm.Parent := nil;
          if mFrame <> nil then
            mFrame.Parent := nil;
         if mysFrame = nil then
           mysFrame := TmysFrame.Create(nil);
         mysFrame.Parent := mainLayout;
         mysFrame.Width := mainLayout.Width;
         mysFrame.Height := mainLayout.Height;
         {$IFDEF Android}
           mysFrame.Rectangle9.Width := mysFrame.Width;
         {$ENDIF}
       end;
  end;
end;

function TmainForm.HandleAppEvent(AAppEvent: TApplicationEvent;
  AContext: TObject): Boolean;
begin
   // APP进入到后台，10秒之内切回到前台，不做二次验证。
  // APP进入到后台，超过10秒切回到前台，再次进行指纹验证。
  // 记录进入后台的时间data 时入后台时检查时间差 10以内不需要验证
  case AAppEvent of
    TApplicationEvent.FinishedLaunching:
     begin
      //  ShowMessage('FinishedLaunching');
     end;
    TApplicationEvent.BecameActive: // 主窗体重新激活也会触发 很频繁一般建议少用
     begin
      // ShowMessage('BecameActive'); // 第一次运行app触发,从后台切换过来也触发
     end;
    TApplicationEvent.WillBecomeInactive:
     begin
    //ShowMessage('WillBecomeInactive')
     end;
    TApplicationEvent.EnteredBackground:
    begin
       //ShowMessage('EnteredBackground')  //切换到后台
       Timer1.Enabled := True;
    end;
    TApplicationEvent.WillBecomeForeground:
    begin
      // ShowMessage('WillBecomeForeground'); // 从后台切换到前台
 //      if Gsettingjson.TimeLimit <> tlNone then
       if timecount>4 then
       begin
         //
         Timer1.Enabled := False;
         timecount := 0;
         startFormShow;

       end;
    end;
    TApplicationEvent.WillTerminate:
    begin
      // ShowMessage('WillTerminate');
    end;
    TApplicationEvent.LowMemory:
    begin
      // ShowMessage('LowMemory');
    end;

    TApplicationEvent.TimeChange:
    begin
     // ShowMessage('TimeChange');
    end;

    TApplicationEvent.OpenURL:
    begin
     // ShowMessage('OpenURL');
    end;

  end;
    Result := True;
end;

procedure TmainForm.i4Click(Sender: TObject);
var a: TSizeF;
 ft : TText;
  I: Integer;
begin
   // image4.MultiResBitmap.Items[1].ID := 1;
//image4.Bitmap. image4.MultiResBitmap[1];
  a.cx := 64;
  a.cy := 64;
  //image4.Bitmap := ImageList1.Bitmap(a,3);

  for I := 0 to 3 do
  begin
    if I = (Sender As TImage).Tag then
    begin
       (Sender As TImage).Bitmap := ImageList1.Bitmap(a,I);
        ft := findComponent('t'+(I+1).ToString) As TText;
          ft.TextSettings.FontColor := TAlphaColorRec.Black;
    end
    else
    begin
        case I+1 of
            1 : begin
                  i1.Bitmap := ImageList2.Bitmap(a,I);
                  t1.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            2 : begin
                  i2.Bitmap := ImageList2.Bitmap(a,I);
                  t2.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            3 : begin
                  i3.Bitmap := ImageList2.Bitmap(a,I);
                  t3.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
            4 : begin
                  i4.Bitmap := ImageList2.Bitmap(a,I);
                  t4.TextSettings.FontColor := TAlphaColorRec.Gray;
                end;
          end;
    end;
  end;
  frameShow((Sender As TImage).Tag);
end;


procedure TmainForm.startFormShow;
var startForm : TstartForm;
begin
  //
  startForm := TstartForm.Create(nil);
   startForm.ShowModal(
     procedure(ModalResult : TModalResult)
       begin
         if ModalResult = mrNone then
          begin
             //
          end;
       end
   );
   FreeAndNil(startForm);
end;

procedure TmainForm.Timer1Timer(Sender: TObject);
begin
   //
   timecount := timecount + 1;
end;

//initialization

end.
