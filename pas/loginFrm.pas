unit loginFrm;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit;

type
  TloginForm = class(TForm)
    Text1: TText;
    Button1: TButton;
    Rectangle1: TRectangle;
    Text2: TText;
    Rectangle2: TRectangle;
    Edit1: TEdit;
    Edit2: TEdit;
    Rectangle3: TRectangle;
    Rectangle4: TRectangle;
    CheckBox1: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure Edit1CanFocus(Sender: TObject; var ACanFocus: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Text2Click(Sender: TObject);
  private
    { Private declarations }
    FFristEditFocus1: Boolean;     //账号输入框
    FFristEditFocus2: Boolean;     //密码输入框
  public
    { Public declarations }
  end;

var
  loginForm: TloginForm;

implementation

{$R *.fmx}

procedure TloginForm.Button1Click(Sender: TObject);
begin
  // Self.CloseModal;
   Self.Close;
end;

procedure TloginForm.Edit1CanFocus(Sender: TObject; var ACanFocus: Boolean);
begin
    //判断输入是否为第一次，若是 则清空提示灰色文字 且设置之后的文字颜色为黑色
//    if (Sender as TEdit).Tag = 1 then
//    begin
//      if FFristEditFocus1 then
//       (Sender as TEdit).Text := '';
//      (Sender as TEdit).TextSettings.FontColor := TAlphaColorRec.Black ;
//    end;
//
//    if (Sender as TEdit).Tag = 2 then
//    begin
//      if FFristEditFocus2 then
//       (Sender as TEdit).Text := '';
//      (Sender as TEdit).TextSettings.FontColor := TAlphaColorRec.Black ;
//    end;

  case (Sender as TEdit).Tag of
     1: begin
         if FFristEditFocus1 then
           (Sender as TEdit).Text := '';
         FFristEditFocus1 := False;
     end;

     2: begin
         if FFristEditFocus2 then
            (Sender as TEdit).Text := '';
         FFristEditFocus2 := False;
     end;
  end;

  (Sender as TEdit).TextSettings.FontColor := TAlphaColorRec.Black ;
end;

procedure TloginForm.FormCreate(Sender: TObject);
begin
  FFristEditFocus1 := True;
  FFristEditFocus2 := True;
end;

procedure TloginForm.Text2Click(Sender: TObject);
begin
   //
end;

end.
