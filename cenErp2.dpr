program cenErp2;

uses
  System.StartUpCopy,
  FMX.Forms,
  mainFrm in 'pas\mainFrm.pas' {mainForm},
  loginFrm in 'pas\loginFrm.pas' {loginForm},
  mainFrame in 'frame\mainFrame.pas' {mFrame: TFrame},
  startFrm in 'pas\startFrm.pas' {startForm},
  EnterUnit in 'unit\EnterUnit.pas',
  deviceTool in 'util\deviceTool.pas',
  orderFrame in 'frame\orderFrame.pas' {Frame1: TFrame},
  countUt in 'frame\countUt.pas' {countFrm: TFrame},
  myFrame in 'frame\myFrame.pas' {mysframe: TFrame},
  TestUt in 'pas\TestUt.pas' {Form1},
  uData in 'dm\uData.pas',
  uDM in 'dm\uDM.pas' {DM: TDataModule},
  InFrm in 'pas\InFrm.pas' {Form2},
  AppInfo in 'unit\AppInfo.pas';

{$R *.res}

begin
  Application.Initialize;
  EnterERP;
 // Application.CreateForm(TForm1, Form1);
  //Application.CreateForm(TForm2, Form2);
  // Application.CreateForm(TForm2, Form2);
  //Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
