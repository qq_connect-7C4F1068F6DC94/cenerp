unit EnterUnit;

interface
uses FMX.PlatForm;
procedure EnterERP;
function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject)   : boolean;

implementation
uses System.StartUpCopy,
  FMX.Forms,startFrm,mainFrm,FMX.Dialogs;


procedure EnterERP;

begin
  //
 // Application.CreateForm(TstartForm,startForm);

//  if initStartFrm('') then
//  begin
//    startForm.Close;
//  end;
  Application.CreateForm(TmainForm,mainForm);

  {$IF ANDROID}
   // 后台事件
  // if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService, IInterface(IFMXApplicationEventService)) then
  //  IFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent);
   {$ENDIF}
  //startForm := TstartForm.Create(nil);
  //startForm.Show;
//  if initStartFrm('')then
 // begin
    //initStartFrm('');
   // Application.CreateForm(TForm1, Form1);
 // end;

end;


function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
begin
     // APP进入到后台，10秒之内切回到前台，不做二次验证。
  // APP进入到后台，超过10秒切回到前台，再次进行指纹验证。
  // 记录进入后台的时间data 时入后台时检查时间差 10以内不需要验证
  case AAppEvent of
    TApplicationEvent.FinishedLaunching:
     begin
        ShowMessage('FinishedLaunching');
     end;
    TApplicationEvent.BecameActive: // 主窗体重新激活也会触发 很频繁一般建议少用
     begin
       ShowMessage('BecameActive'); // 第一次运行app触发,从后台切换过来也触发
     end;
    TApplicationEvent.WillBecomeInactive:
     begin
    ShowMessage('WillBecomeInactive')
     end;
    TApplicationEvent.EnteredBackground:
    begin
       ShowMessage('EnteredBackground')  //切换到后台
    end;
    TApplicationEvent.WillBecomeForeground:
    begin
       ShowMessage('WillBecomeForeground'); // 从后台切换到前台
//      if Gsettingjson.TimeLimit <> tlNone then
//      begin
//
//
//      end;
    end;
    TApplicationEvent.WillTerminate:
    begin
       ShowMessage('WillTerminate');
    end;
    TApplicationEvent.LowMemory:
    begin
       ShowMessage('LowMemory');
    end;

    TApplicationEvent.TimeChange:
    begin
      ShowMessage('TimeChange');
    end;

    TApplicationEvent.OpenURL:
    begin
      ShowMessage('OpenURL');
    end;

  end;
    Result := True;
end;
end.
