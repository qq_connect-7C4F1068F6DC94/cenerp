unit mainFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Layouts;

type
  TmFrame = class(TFrame)
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Image1: TImage;
    Text1: TText;
    BroadCast: TText;
    Rectangle3: TRectangle;
    Layout2: TLayout;
    Text3: TText;
    GridLayout1: TGridLayout;
    Layout3: TLayout;
    Text4: TText;
    Image2: TImage;
    Layout4: TLayout;
    Text5: TText;
    Image3: TImage;
    Layout5: TLayout;
    Text6: TText;
    Image4: TImage;
    Layout6: TLayout;
    Text7: TText;
    Image5: TImage;
    Layout7: TLayout;
    Text8: TText;
    Image6: TImage;
    Layout8: TLayout;
    Text9: TText;
    Image7: TImage;
    Layout9: TLayout;
    Text10: TText;
    Image8: TImage;
    Layout10: TLayout;
    Text11: TText;
    Image9: TImage;
    VertScrollBox1: TVertScrollBox;
    Text12: TText;
    Layout1: TLayout;
    Rectangle4: TRectangle;
    Layout11: TLayout;
    Rectangle5: TRectangle;
    Layout12: TLayout;
    Text13: TText;
    GridLayout2: TGridLayout;
    Layout13: TLayout;
    Text14: TText;
    Image10: TImage;
    Layout14: TLayout;
    Text15: TText;
    Layout15: TLayout;
    Text16: TText;
    Layout16: TLayout;
    Text17: TText;
    Layout17: TLayout;
    Text18: TText;
    Layout18: TLayout;
    Text19: TText;
    Layout19: TLayout;
    Text20: TText;
    Layout20: TLayout;
    Text21: TText;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image17: TImage;
    Line1: TLine;
    Line2: TLine;
    orderCircle: TCircle;
    orderCount: TText;
    procedure Layout8Click(Sender: TObject);
    procedure Layout6DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TmFrame.Layout6DblClick(Sender: TObject);
begin
  orderCirCle.Visible := False;
  orderCount.Text := '0';
end;

procedure TmFrame.Layout8Click(Sender: TObject);
var count:  Integer;
begin
  //
  count := orderCount.Text.ToInteger;
  Inc(count);
  orderCircle.Visible := True;
  ordercount.Text := count.ToString;

  BroadCast.Text := '你有新的未处理订单，请及时处理';
end;

end.
