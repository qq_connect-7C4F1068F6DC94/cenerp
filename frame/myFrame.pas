unit myFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Layouts;

type
  Tmysframe = class(TFrame)
    VertScrollBox1: TVertScrollBox;
    Rectangle12: TRectangle;
    Circle1: TCircle;
    Circle2: TCircle;
    Rectangle13: TRectangle;
    Layout1: TLayout;
    Text20: TText;
    Text21: TText;
    Layout2: TLayout;
    Text22: TText;
    Text23: TText;
    Layout3: TLayout;
    Text24: TText;
    Text25: TText;
    Layout4: TLayout;
    Text26: TText;
    Text27: TText;
    Rectangle10: TRectangle;
    Rectangle11: TRectangle;
    Text9: TText;
    GridPanelLayout1: TGridPanelLayout;
    Rectangle1: TRectangle;
    Image1: TImage;
    Text1: TText;
    Rectangle2: TRectangle;
    Image2: TImage;
    Text2: TText;
    Rectangle3: TRectangle;
    Image3: TImage;
    Text3: TText;
    Rectangle4: TRectangle;
    Image4: TImage;
    Text4: TText;
    Rectangle5: TRectangle;
    Image5: TImage;
    Text5: TText;
    Rectangle6: TRectangle;
    Image6: TImage;
    Text6: TText;
    Rectangle7: TRectangle;
    Image7: TImage;
    Text7: TText;
    Rectangle8: TRectangle;
    Image8: TImage;
    Text8: TText;
    Rectangle15: TRectangle;
    Rectangle16: TRectangle;
    Text10: TText;
    GridPanelLayout2: TGridPanelLayout;
    Rectangle17: TRectangle;
    Image9: TImage;
    Text11: TText;
    Rectangle18: TRectangle;
    Image10: TImage;
    Text12: TText;
    Rectangle19: TRectangle;
    Image11: TImage;
    Text13: TText;
    Rectangle20: TRectangle;
    Image12: TImage;
    Text14: TText;
    Rectangle21: TRectangle;
    Image13: TImage;
    Text15: TText;
    Rectangle22: TRectangle;
    Image14: TImage;
    Text16: TText;
    Rectangle23: TRectangle;
    Image15: TImage;
    Text17: TText;
    Rectangle24: TRectangle;
    Image16: TImage;
    Text18: TText;
    Text19: TText;
    Rectangle9: TRectangle;
    Layout5: TLayout;
    Layout6: TLayout;
    Text28: TText;
    procedure Circle2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
uses loginFrm;
{$R *.fmx}

procedure Tmysframe.Circle2Click(Sender: TObject);
var loginForm: TLoginForm;
begin

  loginForm := TLoginForm.Create(nil);
  loginForm.Show;
end;

end.
