unit deviceTool;
{
   设备工具类

}
interface
uses FMX.PlatForm,
      {$IFDEF ANDROID}
     Androidapi.Helpers,
      Androidapi.JNI.GraphicsContentViewText,
 {$ENDIF}
 System.Types;
var  screenwidth: Single;
     screenHeight: Single;
function getScreen(checked: boolean):boolean;
function setBarColor(checked: boolean):boolean;
implementation

function setBarColor(checked: boolean):boolean;
begin
  {$IFDEF ANDROID}
    TAndroidHelper.Activity.getWindow.setStatusBarColor($FFFEDB5A);
  {$ENDIF}
end;
function getScreen(checked: boolean): boolean;
var
  ScreenSvc: IFMXScreenService;
  Size: TPointF;
  iw: Integer;
begin
  if TPlatformServices.Current.SupportsPlatformService  (IFMXScreenService, IInterface(ScreenSvc))  then
  begin
    Size := ScreenSvc.GetScreenSize;
    screenwidth := Size.X;
    screenHeight := Size.Y;
    iw := Trunc(screenwidth) mod 4;
    if iw = 0 then
    begin
      Result := True;
    end
    else
    begin
      Result := False;
    end;
  end
  else
  begin
     screenwidth := 0;
    screenHeight := 0;
    Result := False;
  end;
end;
end.
