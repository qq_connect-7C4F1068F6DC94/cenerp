USE [cenerp]
GO

/****** Object:  Table [dbo].[files]    Script Date: 2020/4/20 23:01:02 
Uid  int 
Username string
uPassword string
uWxid string
uWxname string
uWxpic string
Ubz string
uCreate_time datatime
uLast_time      datetime
uStatus int
uDel      int
uOnline int
uBy1 string
uBy2 string

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[user](
	[uid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](max) NULL,
	[password] [varchar](max) NOT NULL,
	[wxid] [varchar](max) ,
	[wxname] [varchar](max) ,
	[wxpic] [varchar](max) ,
	[bz] [varchar](max) ,
	[create_time] [datetime] NOT NULL,
	[last_time] [datetime] NOT NULL,
	[status] [int] default 0,
	[del] [int] default 0
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



