USE [cenerp]
GO

/****** Object:  Table [dbo].[files]    Script Date: 2020/4/20 23:01:02 


******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[page](
	[pid] [int] IDENTITY(1,1) NOT NULL,
	[pagename] [varchar](max) NULL,
	[pageact] [varchar](max) NOT NULL,
	[del] [int] default 0
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


